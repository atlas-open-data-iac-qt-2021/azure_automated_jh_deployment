**[Technical Overview](#technical-overview)** |
**[Structure](#structure)** |
**[Spawner: Prepare the Jupyter Notebook Image](#spawner-prepare-the-jupyter-notebook-image)** |
**[Deploy JupyterHub on an AWS EC2 instance](#deploy-jupyterhub-on-an-aws-ec2-instance)** 

# Disclaimer

This repository is inspired to [the official Jupyterhub work](https://github.com/jupyterhub/jupyterhub-deploy-docker), so in order to have further clarifications, please feel free to explore it.

# JupyterHub deployer

This deployer provides a reference implementation of [JupyterHub](https://github.com/jupyter/jupyterhub), a
multi-user [Jupyter Notebook](http://jupyter.org/) environment, on a
**single host** using [Docker](https://docs.docker.com).  

Possible **use cases** include:

* Creating a JupyterHub demo environment that you can spin up relatively
  quickly.
* Providing a multi-user Jupyter Notebook environment for small classes,
  teams, or departments.


## Technical Overview

Key components of this reference deployment are:

* **Host**: Runs the [JupyterHub components](https://jupyterhub.readthedocs.org/en/latest/getting-started.html#overview)
  in a Docker container on the host.

* **Authenticator**: Uses [Native Authenticator](https://github.com/jupyterhub/nativeauthenticator) to authenticate users.

* **Spawner**:Uses [DockerSpawner](https://github.com/jupyter/dockerspawner)
  to spawn single-user Jupyter Notebook servers in separate Docker
  containers on the same host.

* **Persistence of Hub data**: Persists JupyterHub data in a Docker
  volume on the host.

* **Persistence of user notebook directories**: Persists user notebook
  directories in Docker volumes on the host.


## Structure

### Docker

This deployment uses Docker, via [Docker Compose](https://docs.docker.com/compose/overview/), for all the things.
[Docker Engine](https://docs.docker.com/engine) 1.12.0 or higher is
required.

### HTTPS and SSL/TLS certificate

This deployment configures JupyterHub to use HTTPS. Since it exploits the DNS provided by AWS, the infrastructure is by default accessible via IP address; nevertheless **it is possible to redirect the instance** to a custom domain name that you wish to use for JupyterHub, for example, `myfavoritesite.com` or `jupiterplanet.org`.


### Authenticator

This deployment uses Native Authenticator to authenticate users.

Native Authenticator provides the following features:

*  New users can signup on the system;

*  New users can be blocked of accessing the system and need an admin authorization;

*  Option of increase password security by avoiding common passwords or minimum password length;

*  Option to block users after a number attempts of login;

*  Option of open signup and no need for initial authorization;

*  Option of adding more information about users on signup.

By default, the user "admin" has administrator privileges. Once logged in, this user will have the ability to manage new users registrations through JupyterHub's admin console at https://jupyterhub_ip_address/hub/authorize.

## Spawner: Prepare the Jupyter Notebook Image

You can configure JupyterHub to spawn Notebook servers from any Docker image, as
long as the image's `ENTRYPOINT` and/or `CMD` starts a single-user instance of
Jupyter Notebook server that is compatible with JupyterHub.

Whether you build a custom Notebook image or pull an image from a public or
private Docker registry, the image must reside on the host.  

If the Notebook image does not exist on host, Docker will attempt to pull the
image the first time a user attempts to start his or her server.  In such cases,
JupyterHub may timeout if the image being pulled is large, so it is better to
pull the image to the host before running JupyterHub.  

This deployment defaults to the
[ROOT notebook](https://hub.docker.com/r/atlasopendata/root_notebook/)
Notebook image, which is built on top of the `scipy-notebook`
[Docker stacks](https://github.com/jupyter/docker-stacks).



## Deploy JupyterHub on an AWS EC2 instance

**DISCLAIMER**: this operations work best on **Linux**; we recommend to such an OS to setup the infrastructure.
### Prerequisites 
It is required to install the AWS CLI and the Terraform CLI; as a backup, the setup script will initialize anyway the installation of both, but it is preferred to have them pre-installed.
#### Install Terraform CLI
To install terraform, execute the following commands (you need to have SUDO privileges):
```
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get --yes install terraform
```
#### Install AWS CLI
To install AWS CLI, execute the following commands (you need to have SUDO privileges):
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```
If you don't have `unzip` installed, please run `sudo apt --yes install unzip`.

### Configuration and Deployment

To operatively deploy the JupyterHub server on top of an AWS VM, you need to follow a few steps:

1.  Clone this repository in a directory of your choice on your PC, by executing one of the two commands below:
```
git clone https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/aws_automated_jh_deployment.git
```
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-open-data-iac-qt-2021/aws_automated_jh_deployment.git
```

2.  Fill the `config.ini` file, which contains the following fields:
    * `username`: the name that will be created for the user in the instance.

    * `password`: the password for the instance created. To be used together with username in case of direct ssh connection.

    * `sshkey`: the name of the ssh key created for the instance.

    - `image`: the AMI (Amazon Machine Image) of the VM created in AWS. You can inspect the different AMIs in your AWS Dashboard. The images tested for this deployement are:
        * Ubuntu 18.04: ami-09a1e275e350acf38
        * Ubuntu 20.04: ami-0bd1bb604d9268948

    * `flavor`: the flavor of the VM created in AWS. This parameter encodes the resources available for the instance, such as CPU, GPU, memory, etc. The list of available flavors can be found at https://aws.amazon.com/ec2/instance-types.

    * `dnsrecord`: [optional] the domain to be used for the certificate. As mentioned before, it is not necessary; if you possess a valid domain, this variable will produce the related certificate and will set up the accessibility.

    * `notebook`: the image of the notebook that will be used. By default, this parameter points to [atlasopendata/root_notebook](https://hub.docker.com/r/atlasopendata/root_notebook/). You can insert any custom image compatible with JupyterHub. 
     **DISCLAIMER**: if you use a `t2.micro/t3.micro` instance (free tier AWS computing), we recommend to use a light image such as the [jupyter/base-notebook](https://hub.docker.com/r/jupyter/base-notebook/), in order not to exceed the disk space avilable on the instance.

    * `security_group`: the name of the security group created for the AWS instance. This parameter will set-up the rules for incoming connections (SSH, HTTPS).

  3. Run the `setup.sh` script by executing
  ```
  source setup.sh
  ```
  As a first operation, you will configure the AWS service: follow the prompts to input your AWS Access Key ID and Secret Access Key, which you'll find on [this page](https://console.aws.amazon.com/iam/home?#security_credential). 
Additional informations are required, as the region or the output format; the latter is normally the `json` format, while a full list of the available regions is reported [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-regions).
Finally, some more parameters are required to complete the configuration. Below all the necessary attributes are listed:
- AWS Access Key ID
- Secret Access Key
- Region
- Output format

  Then, a summary of the whole configuration will be displayed for you to confirm. Once done that, terraform will begin the deployment process, and will return the URL of the Hub at the end of the procedure.

---

