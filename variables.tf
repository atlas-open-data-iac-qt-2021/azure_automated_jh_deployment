variable "username" {
  description = "The username for the DB master user"
  type        = string
}

variable "password" {
  description = "The password for the DB master user"
  type        = string
}

#variable "hostname" {
#  description = "The name of the VM created in openstack"
#  type        = string
#}

variable "security_group" {
  description = "The security group of the VM created in aws"
  type        = string
}

# variable "sshkey" {
#   description = "The name of the ssh key created for the instance"
#   type        = string
# }

variable "size" {
  description = "The size of the VM used for the instance"
  type        = string
}

variable "publisher" {
  description = "Publisher of the image"
  type        = string
}

variable "offer" {
  description = "Offer of the image"
  type        = string
}

variable "sku" {
  description = "Stock-keeping-Unit"
  type        = string
}

variable "os_version" {
  description = "Version of the image"
  type        = string
}

variable "region" {
  description = "The name of the region"
  type        = string
}






