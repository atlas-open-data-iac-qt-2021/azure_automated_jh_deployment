echo ""
cat misc/title.txt
echo ""
if [[ $(az --version) ]]; then
    echo ""
else
    echo "installing the Azure CLI..."
	curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
	
fi

echo "Configuring the Azure provider..."
#az login
az account show


export MSYS_NO_PATHCONV=1

# Set the color variable
green='\033[0;32m'
red='\e[1;31m'
# Clear the color after that
clear='\033[0m'

if [ -a user_data.txt ]
then 
 rm user_data.txt
fi


echo ""
echo "===================================================================="
service_principal=$(sed -nr "/^\[ATLAS\]/ { :l /^service_principal[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export service_principal=${service_principal//[[:blank:]]/}
echo -e "Name for the user:  ${red}$service_principal${clear}";

echo ""
echo "===================================================================="
TF_VAR_username=$(sed -nr "/^\[INSTANCE\]/ { :l /^username[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_username=${TF_VAR_username//[[:blank:]]/}
echo -e "Name for the user:  ${red}$TF_VAR_username${clear}";

echo ""
echo "===================================================================="
TF_VAR_password=$(sed -nr "/^\[INSTANCE\]/ { :l /^password[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_password=${TF_VAR_password//[[:blank:]]/}
echo -e -n "Password for the instance created:  ${red}$TF_VAR_password${clear}";


echo ""
echo "===================================================================="
TF_VAR_size=$(sed -nr "/^\[INSTANCE\]/ { :l /^size[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_size=${TF_VAR_size//[[:blank:]]/}
echo -e "The size of the VM used for the instance:  ${red}$TF_VAR_size${clear}";
echo "------------------------------------------"
echo "Standard_B1s     (Azure free tier)"
echo "------------------------------------------"

echo ""
echo "===================================================================="
TF_VAR_publisher=$(sed -nr "/^\[IMAGE\]/ { :l /^publisher[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_publisher=${TF_VAR_publisher//[[:blank:]]/}
echo -e "Publisher of the image:  ${red}$TF_VAR_publisher${clear}";
echo "------------------------------------------"
TF_VAR_offer=$(sed -nr "/^\[IMAGE\]/ { :l /^offer[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_offer=${TF_VAR_offer//[[:blank:]]/}
echo -e "Type of image:  ${red}$TF_VAR_offer${clear}";
echo "------------------------------------------"
TF_VAR_sku=$(sed -nr "/^\[IMAGE\]/ { :l /^sku[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_sku=${TF_VAR_sku//[[:blank:]]/}
echo -e "Stock-keeping-Unit:  ${red}$TF_VAR_sku${clear}";
echo "------------------------------------------"
TF_VAR_os_version=$(sed -nr "/^\[IMAGE\]/ { :l /^version[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_os_version=${TF_VAR_os_version//[[:blank:]]/}
echo -e "Version of the image:  ${red}$TF_VAR_os_version${clear}";


echo ""
echo "===================================================================="
TF_VAR_security_group=$(sed -nr "/^\[CONNECTION\]/ { :l /^security_group[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_security_group=${TF_VAR_security_group//[[:blank:]]/}
echo -e "Security group used:  ${red}$TF_VAR_security_group${clear}";


echo ""
echo "===================================================================="
#export TF_VAR_notebook_image=$(awk -F "=" '/notebook/ {print $2}' config.ini)
DOCKER_NOTEBOOK_IMAGE=$(sed -nr "/^\[JUPYTERHUB\]/ { :l /^notebook[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
DOCKER_NOTEBOOK_IMAGE=${DOCKER_NOTEBOOK_IMAGE//[[:blank:]]/}
replacement=$(sed -nr "/ / { :l /^DOCKER_NOTEBOOK_IMAGE[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./conf/.env)
replacement=${replacement//[[:blank:]]/}
sed -i "s@$replacement@$DOCKER_NOTEBOOK_IMAGE@g" ./conf/.env
echo -e "Docker image used for the notebooks:  ${red}$DOCKER_NOTEBOOK_IMAGE${clear}";

echo ""
echo "===================================================================="
TF_VAR_region=$(sed -nr "/^\[CONNECTION\]/ { :l /^region[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./config.ini)
export TF_VAR_region=${TF_VAR_region//[[:blank:]]/}
echo -e "Region used:  ${red}$TF_VAR_region${clear}";



az ad sp create-for-rbac --name "${service_principal}" --role Contributor > secret_service.txt

export ARM_SUBSCRIPTION_ID=$(az account show --query id --output tsv)

azure_subscription_tenant_id=`awk 'NR==6' secret_service.txt`
export ARM_TENANT_ID=$( cut -d'"' -f4 <<<"$azure_subscription_tenant_id")

service_principal_appid=`awk 'NR==2' secret_service.txt`
export ARM_CLIENT_ID=$( cut -d'"' -f4 <<<"$service_principal_appid")

service_principal_password=`awk 'NR==5' secret_service.txt`
export ARM_CLIENT_SECRET=$( cut -d'"' -f4 <<<"$service_principal_password")

echo ""
echo "===================================================================="
echo "Printout of the ARM variables:"
printenv | grep ^ARM*




#****************************************** Create the conf file user_data.txt
echo "#cloud-config">>user_data.txt
echo "password: ${TF_VAR_password}">>user_data.txt
echo "chpasswd: { expire: False }">>user_data.txt
echo "ssh_pwauth: True">>user_data.txt


echo ""
echo "===================================================================="
echo "===================================================================="
read -r -p "Do you confirm this configuration? (yes/no) " confirmation


if [ $confirmation == 'yes' ]
then 
 if ! [[ $(terraform -version) ]]; then
	sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
	curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
	sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
	sudo apt-get update && sudo apt-get --yes install terraform
 fi	
 echo "Commencing deployment..."
 terraform 0.13upgrade .
 terraform init
 terraform plan
 terraform apply

fi
